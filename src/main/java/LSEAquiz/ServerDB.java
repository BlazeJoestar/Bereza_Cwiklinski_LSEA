package LSEAquiz;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Server object that will wait for one Client to connect,
 * read input from it, and send back proper database object
 */
public class ServerDB {
    /**
     * Main method that does everything a server has to do
     * @param args arguments passed int the terminal
     */
    public static void main(String[] args) {
        // try-with-resources that starts the server
        try (ServerSocket server = new ServerSocket(56534)) {
            // if succeeded, we print the message, and continue
            System.out.println("Starting to listen");
            while (true) {
                // try-with-resources that listens on the socket
                try (Socket socket = server.accept()) {
                    // creating the input stream "bridge"
                    InputStream inputStream = socket.getInputStream();
                    ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
                    // reading the input
                    int request = (int) objectInputStream.readObject();

                    // if input matches some options, we will process
                    if(request == 28){
                        // we read the data written in the .txt file
                        QuestionsDB database = new QuestionsDB("questions3txt", true);
                        // creating the output stream
                        OutputStream outputStream = socket.getOutputStream();
                        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
                        // now we send the serialized database object
                        System.out.println("Sending database to client");
                        objectOutputStream.writeObject(database);
                    }
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}